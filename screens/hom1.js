import React, { Component } from "react";
import { Alert, View, Image, Button, Text,TouchableHighlight, StyleSheet, Keyboard } from "react-native";
import { TextInput } from "react-native-gesture-handler";



         export default class Home extends Component {
            constructor (props) {
                super(props);
                this.state = {
                  Email: '',
                  password: '',
                  };
             }
            val()
            
            static navigationOptions = {
                
    
              headerTitle: <Image
              source={require('../assets/spiro.png')}
              style={{ width: 30, height: 30 }}
              
            />, 
              
              headerRight: (
                <Button
                onPress={()=> alert ('This is button')}
                title="Info"
                color="red"
                />
              )
              
            };
           render() {
            
              return (
                <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
             
             placeholder="e.g: abc@example.com" 
             onChangeText={
              emailText => this.setState({Email: emailText})
             }/>
        </View>
        
        <View style={styles.inputContainer}>
          <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/>
          <TextInput style={styles.inputs}
              
              placeholder="Password"
              onChangeText={
                password => this.setState({password})
              } />
        </View>
                  <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} 
                    onPress={() => {
                       
                       this.props.navigation.navigate('Details',
                      {

                        password:this.state.password,
                          Email:this.state.Email,
                          
                      });
                    }}
                  ><Text style={styles.loginText}>Login</Text>
                  </TouchableHighlight>
          
                </View>
              );
            }
          }
          const styles = StyleSheet.create({
            container: {
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#DCDCDC',
            },
            inputContainer: {
                borderBottomColor: '#F5FCFF',
                backgroundColor: '#FFFFFF',
                borderRadius:30,
                borderBottomWidth: 1,
                width:250,
                height:45,
                marginBottom:20,
                flexDirection: 'row',
                alignItems:'center'
            },
            inputs:{
                height:45,
                marginLeft:16,
                borderBottomColor: '#FFFFFF',
                flex:1,
            },
            inputIcon:{
              width:30,
              height:30,
              marginLeft:15,
              justifyContent: 'center'
            },
            buttonContainer: {
              height:45,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom:20,
              width:250,
              borderRadius:30,
            },
            loginButton: {
              backgroundColor: "#00b5ec",
            },
            loginText: {
              color: 'white',
            }
          });